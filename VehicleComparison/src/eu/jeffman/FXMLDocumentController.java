/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman;

import eu.jeffman.listener.VehicleListener;
import eu.jeffman.model.IVehicle;
import eu.jeffman.model.VehicleRepository;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.web.WebView;
import javafx.util.Callback;

/**
 *
 * @author 874941
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML private Label label;
    
    @FXML private ListView vehicleList;
    
    @FXML private WebView webView;
    
    @FXML private TabPane tabPane;
    
    @FXML private Tab overViewTab;
    
    @FXML private Tab webTab;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        VehicleRepository repo = VehicleRepository.getInstance();
        final ObservableList<IVehicle> observerList = FXCollections.observableList(repo.getVehicles());
        vehicleList.setCellFactory(new Callback<ListView<IVehicle>, ListCell<IVehicle>>(){
            @Override
            public ListCell<IVehicle> call(ListView<IVehicle> listView){
                VehicleItemCell cell = new VehicleItemCell(new VehicleListener(){

                    @Override
                    public void WebLinkAction(String webLink) {
                        SetWebLink(webLink);
                    }
                    
                });
                return cell;
            }
        });
        
        vehicleList.setItems(observerList);
        
    }    
    
    private void SetWebLink(String url){
        SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();
        webView.getEngine().load(url);
        selectionModel.select(webTab);
    }
}
