/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman;

import eu.jeffman.listener.VehicleListener;
import eu.jeffman.model.IVehicle;
import javafx.scene.control.ListCell;

/**
 *
 * @author 874941
 */
public class VehicleItemCell extends ListCell<IVehicle> {
    private VehicleListener vehicleListener;
    private VehicleItem vItem;
    
    /**
     * Creates a new VehicleItemCell with the specified listener.
     * @param listener The specified listener.
     */
    public VehicleItemCell(VehicleListener listener){
        vehicleListener = listener;
    }
    
    @Override
    public void updateItem(IVehicle item, boolean empty)
    {
        super.updateItem(item, empty);
        if(item!= null){
            vItem = new VehicleItem(item);
            vItem.setVehicleListener(vehicleListener);
            setGraphic(vItem.getBox());
        }
    }    

    /**
     * @return the vehicleListener
     */
    public VehicleListener getVehicleListener() {
        return vehicleListener;
    }

    /**
     * @param vehicleListener the vehicleListener to set
     */
    public void setVehicleListener(VehicleListener vehicleListener) {
        this.vehicleListener = vehicleListener;
        vItem.setVehicleListener(vehicleListener);
    }
}
