/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman;

import eu.jeffman.listener.VehicleListener;
import eu.jeffman.model.IVehicle;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 *
 * @author 874941
 */
public class VehicleItem {
    @FXML
    public Label nameField;
    
    @FXML
    public Label typeField;
    
    @FXML
    public HBox hBox;
    
    /**
     *  The current vehicle this item describes
     */
    private IVehicle vehicle;
    
    /**
     * The vehicle listener for sending the events to.
     */
    private VehicleListener vehicleListener;
    
    /**
     * Loads the FXML and sets the information to vehicle
     * @param vehicle 
     */
    public VehicleItem(IVehicle vehicle){
        this();
        setVehicle(vehicle);
    }
    
    /**
     * Creats the vehicle item by loadin the fxml and controller.
     */
    public VehicleItem(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("VehicleItem.fxml"));
        fxmlLoader.setController(this);
        
        try{
            fxmlLoader.load();
        }
        catch(IOException ioe){
            throw new RuntimeException(ioe);
        }
    }
    
    /**
     * Sets the information of this item to vehicle
     * @param vehicle 
     */
    public void setVehicle(IVehicle vehicle){
        nameField.setText(vehicle.getName());
        typeField.setText(vehicle.toString());
        this.vehicle = vehicle;
    }
    
     /**
      * Gets the current selected vehicle
     * @return the current vehicle
     */
    public IVehicle getVehicle() {
        return vehicle;
    }
    
    /**
     * Returns the HBox for adding to the listview.
     * @return 
     */
    public HBox getBox(){
        return hBox;
    }
    
    /**
     * Handles the event when the "Visit Web" is clicked
     * @param event 
     */
    @FXML
    public void handleWebRequest(ActionEvent event){
        if(vehicleListener != null){
            vehicleListener.WebLinkAction(vehicle.getWebLink());
        }
    }

    /**
     * @param vehicleListener the vehicleListener to set
     */
    public void setVehicleListener(VehicleListener vehicleListener) {
        this.vehicleListener = vehicleListener;
    }


}
