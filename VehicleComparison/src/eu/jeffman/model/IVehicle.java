/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman.model;

/**
 * Deze interface bevat methodes voor de verschillende voertuigen
 * @author Master Djef
 */
public interface IVehicle extends Comparable<IVehicle> {
    /**
     * Returns the name of the vehicle
     * @return 
     */
    String getName();
    
    /**
     * Returns the engine
     * @return An IEngine interface which represents the current engine.
     */
    IEngine getEngine();
    
    /** 
     * Gets the number of wheels
     * @return An integer with the number of wheels
     */
    int getNumberOfWheels();
    
    /**
     * Gets the link to more information on this vehicle
     * @return A String with the url.
     */
    String getWebLink();
}
