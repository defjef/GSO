/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman.model;

/**
 *
 * @author 874941
 */
public class GasEngine implements IEngine {
    /**
     * The amount of horse power in the engine.
     */
    private int power;
    
    /** 
     * Number of pistons in the engine. The minimum amount is 1 the maximum amount is 16
     */
    private int numberOfPistons;
    
    /**
     * Size of the engine in cc minimum amount is 50. Maxumimum amount 12000.
     */
    private double size;
    
    public GasEngine(int power, int pistons, double size){
        this.power = power;
        this.numberOfPistons = pistons;
        this.size = size;
    }
    
    @Override
    public int compareTo(IEngine o) {
        if(o instanceof GasEngine){
            GasEngine gas = (GasEngine)o;
            if((gas.getNumberOfPistons() == getNumberOfPistons())
                    && (gas.getSize() == getSize())) {
                return 0;
            }
            else{
                int foreign = (int)(gas.getNumberOfPistons() + gas.getSize());
                int local = (int)(getNumberOfPistons() + getSize());
                return foreign - local;
                
            }
        }
        else{
            return Integer.MAX_VALUE;
        }
    }

    /**
     * @return the numberOfPistons
     */
    public int getNumberOfPistons() {
        return numberOfPistons;
    }

    /**
     * @param numberOfPistons the numberOfPistons to set
     */
    public void setNumberOfPistons(int numberOfPistons) {
        this.numberOfPistons = numberOfPistons;
    }

    /**
     * @return the size
     */
    public double getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(double size) {
        this.size = size;
    }

    /**
     * @return the power
     */
    public int getPower() {
        return power;
    }

    /**
     * @param power the power to set
     */
    public void setPower(int power) {
        this.power = power;
    }
    
    @Override
    public String toString(){
        return String.format("Gas: %f cc, %d pistons.", size, numberOfPistons);
    }
}
