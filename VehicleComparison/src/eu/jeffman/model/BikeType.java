/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman.model;

/**
 *
 * @author 874941
 */
public enum BikeType {
    Racer,
    Chopper,
    Cruiser,
    CafeRacer
}
