/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman.model;

/**
 *
 * @author 874941
 */
public abstract class Bike implements IVehicle {
    private String name;
    private IEngine engine;
    private String webLink;
    private BikeType type;

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the engine
     */
    @Override
    public IEngine getEngine() {
        return engine;
    }

    /**
     * @param engine the engine to set
     */
    public void setEngine(IEngine engine) {
        this.engine = engine;
    }

    /**
     * @return the webLink
     */
    @Override
    public String getWebLink() {
        return webLink;
    }

    /**
     * @param webLink the webLink to set
     */
    public void setWebLink(String webLink) {
        this.webLink = webLink;
    }
    
    /**
     * Gets the number of wheels which is always 2 for a bike.
     * @return The number of wheels which is always 2.
     */
    @Override
    public int getNumberOfWheels(){
        return 2;
    }    
    
    /**
     * Gets the type of the bike as an BikeType
     * @return A BikeType enum with current bike type
     */
    public BikeType getType() {
        return type;
    }

    /**
     * Sets the bike type
     * @param type A BikeType enum with current bike type
     */
    public void setType(BikeType type) {
        this.type = type;
    }

    @Override
    public String toString(){
        return String.format("Bike : %s", type);
    }
    /**
     * Compares the cars based on their properties. The importance is :
     * 1) Name
     * 2) Type
     * 3) Engine
     * 4) Weblink
     * 
     * For each property that matches a point will be given and the accumulation will be returned.
     * 
     * @param object
     * @return if object is not of type Car then -1 else the number of properties
     * that will match.
     */
    @Override
    public int compareTo(IVehicle object){
        if(object instanceof Bike){
            // Code to compare cars
            int score = 0;
            
            return score;
        }
        else {
            return -1;
        }
    }
}
