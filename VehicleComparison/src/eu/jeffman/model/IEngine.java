/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman.model;

/**
 * Compares 2 engine by comparing some properties of the engines. 
 * @author 874941
 */
public interface IEngine extends Comparable<IEngine>{
    int getPower();
}
