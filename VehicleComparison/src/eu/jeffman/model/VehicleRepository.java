/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 874941
 */
public class VehicleRepository {
    private static VehicleRepository local = null;
    
    private List<IVehicle> vehicles = new ArrayList<IVehicle>();
    
    public static VehicleRepository getInstance(){
        if(local == null){
            local = new VehicleRepository();
        }
        
        return local;
    }
    
    private VehicleRepository(){
        createList();
    }
    
    /**
     * creates the list of vehicles;
     */
    private void createList(){
        // Tesla Model X
        getVehicles().add(new Car(){
            {
                setName("Tesla Model X P90D");
                setEngine(new ElectricEngine(193, 400, 375 ));
                setWebLink("https://www.teslamotors.com/nl_NL/modelx");
            }
        });
        getVehicles().add(new Car(){
            {
                setName("Nissan Leaf");
                setEngine(new ElectricEngine(80, 400, 172 ));
                setWebLink("https://www.nissan.nl/voertuigen/nieuw/leaf.html");
            }
        });
        getVehicles().add(new Car(){
            {
                setName("Welder up train car");
                setEngine(new GasEngine(1250, 8, 12000));
                setWebLink("http://www.welderup.com/builds/train-car/");
            }
        });
        getVehicles().add(new Bike(){
            {
                setName("Yamaha YZF-R1 WE R1");
                setWebLink("http://www.yamaha-motor.eu/nl/producten/motoren/supersport/yzf-r1.aspx");
                setEngine(new GasEngine(200, 4, 998));
                setType(BikeType.Racer);
            }
        });
        getVehicles().add(new Trike(){
            {
                setName("Intruder 1800");
                setWebLink("http://www.edeltrikes.de/ordner/verkauf/2012/intrud_1800/intrud_1800.html");
                setEngine(new GasEngine(114, 2, 1783));
                setNumberOfSeats(2);
            }
        });
        getVehicles().add(new Trike(){
            {
                setName("Frogman's Rocket II");
                setWebLink("http://torqueandtread.com/motorcycles/the-one-and-only-hemi-v8-powered-rocket-ii-1200hp-trike-motorcycle/");
                setEngine(new GasEngine(1000, 8, 6275));
                setNumberOfSeats(1);
            }
        });
    }

    /**
     * @return the vehicles
     */
    public List<IVehicle> getVehicles() {
        return vehicles;
    }
 
    public List<IVehicle> search(IVehicle terms){
        List<IVehicle> results = new ArrayList<IVehicle>();
        
        // Todo: Create some sort of search logic
        
        return results;
    }
}
