/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.jeffman.model;

/**
 *
 * @author 874941
 */
public class ElectricEngine implements IEngine {

    /**
     * Number of kw in this engine
     */
    private int power;

    /**
     * The operation voltage
     */
    private int voltage;

    /**
     * The action radius on cruising speed.
     */
    private double radius;
    
    public ElectricEngine(int power, int voltage, double radius){
        this.power = power;
        this.voltage = voltage;
        this.radius = radius;
    }

    /**
     * Compares the power and the action radius and returns a ratio.
     *
     * @param o
     * @return
     */
    @Override
    public int compareTo(IEngine o) {
        if (o instanceof ElectricEngine) {
            ElectricEngine electric = (ElectricEngine) o;
            int foreign = (int) (electric.getPower() + electric.getRadius());
            int local = (int) (electric.getPower() + electric.getRadius());

            return foreign - local;
        } else {
            return -1;
        }
    }

    /**
     * *
     * Gets the power of this engine.
     *
     * @return The power in kWh
     */
    @Override
    public int getPower() {
        return power;
    }

    /**
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * @param radius the radius to set
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * @param power the power to set
     */
    public void setPower(int power) {
        this.power = power;
    }

    /**
     * @return the voltage
     */
    public int getVoltage() {
        return voltage;
    }

    /**
     * @param voltage the voltage to set
     */
    public void setVoltage(int voltage) {
        this.voltage = voltage;
    }
    
    @Override
    public String toString(){
        return String.format("Electric: %dkWh, radius: %fkm.", power, radius);
    }
}
